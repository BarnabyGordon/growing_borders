function Settlements() {
	for (i = 0; i< (settlement_count - place_coords.length); i++) {
		x = getRandomInt(0, 200);
		y = getRandomInt(0, 175);
		if (json[y - dY][x - dX] >= 90) {
			place_coords.push([x,y]);
			influence_coords.push(InfluenceCoords(x, y, influence));
		};
	};
};

function DrawPlace(x, y) {
	for (i = 0; i< place_coords.length; i+= 1) {
		xVal = place_coords[i][0];
		yVal = place_coords[i][1];
		if (x == xVal && y == yVal) {
			return 'True';
		};
	};
};

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function Grow(place, x1, y1, radius) {
	coords = [];
	var prev_coord = [2, 2];
	for (i = 0; i<360; i+= 0.5) {
		angle = i * Math.PI / 180;
		offset = 0;
		odd = false;
		var x2 = Math.round(((Math.cos(angle)*radius) + (x1)) *1)/1;
		var y2 = Math.round(((Math.sin(angle)*radius) + (y1)) *1)/1;

		if ((x2 - dX) <= 1 || (x2 - dX) >= 200 || (y2 - dY) >= 200 || (y2 - dY) <= 1) {
			var z = 100;
		} else {
			var z = (json[y2 - dY][x2 - dX]);
		}

		if (z <= sea_level) {
			var radius2 = radius / 0.75;
			var x2 = Math.round(((Math.cos(angle)*radius2) + (x1)) *1)/1;
			var y2 = Math.round(((Math.sin(angle)*radius2) + (y1)) *1)/1;
			console.log(radius2)
		}

		if (x2 != prev_coord[0] || y2 != prev_coord[1]) {
			coords.push([x2, y2]);
			prev_coord = [x2, y2];
		};
	};
	return coords;
};


function InfluenceCoords(x1, y1, radius) {
	coords = [];
	var prev_coord = [2, 2];
	for (i = 0; i<360; i+= 0.5) {
		angle = i * Math.PI / 180;
		x2 = Math.round(((Math.cos(angle)*radius) + (x1)) *1)/1;
		y2 = Math.round(((Math.sin(angle)*radius) + (y1)) *1)/1;

		if (x2 != prev_coord[0] || y2 != prev_coord[1]) {
			coords.push([x2, y2]);
			prev_coord = [x2, y2];
		};
	};
	return coords;
};
