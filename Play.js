function moveScreen(x, y) {
	if (x >= 950 && dX >= -150) {
		dX = dX - 1;
		DrawMap();
	};
	if (x <= 50 && dX <= -1) {
		dX = dX + 1;
		DrawMap();
	};
	if (y >= 450 && dY >= -150) {
		dY = dY - 1;
		DrawMap();
	};
	if (y <= 50 && dY <= -1) {
		dY = dY + 1;
		DrawMap();
	};
};

function LocPopup(x, y) {
	mX = Math.floor(x/20) - dX;
	mY = Math.floor(y/20) - dY;
	for (i = 0; i<place_coords.length; i++) {
		if (mX == place_coords[i][0] && mY == place_coords[i][1]) {
			DrawMap();
			ctx.font="30px Ariel";
			ctx.fillStyle = 'orangered';
			ctx.fillText("Barnadale",x,y);
		};
	};
};

function getMousePos(canvas, evt) {
    var rect = c.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
};

function PlaceInfl() {
	var ctx = c.getContext("2d");
	for (i = 0; i< place_coords.length; i++) {
		ctx.beginPath();
		for (j = 0;j< influence_coords[i].length; j++) {
			var x = (influence_coords[i][j][0] + dX)*20;
			var y = (influence_coords[i][j][1] + dY)*20;
			ctx.lineTo(x, y);
			ctx.strokeStyle = 'orange';
			ctx.lineWidth = '5';
		};
		ctx.stroke();
	};
};

function test_inside(x, y) {
	var result = 0;
	for (i = 0;i< influence_coords.length; i++) {
		var point = [ x, y ];
		if (inside(point, influence_coords[i]) == true) {
			return true;
		};
	};
};

function inside(point, vs) {
    // ray-casting algorithm based on
    // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

    var x = point[0], y = point[1];

    var inside = false;
    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
        var xi = vs[i][0], yi = vs[i][1];
        var xj = vs[j][0], yj = vs[j][1];

        var intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }
    return inside;
};

canvas.addEventListener('mousemove', function(evt) {
    var mousePos = getMousePos(c, evt);
    var message = (mousePos.x - dX) + ',' + (mousePos.y - dY);
    moveScreen(mousePos.x, mousePos.y);
    LocPopup(mousePos.x, mousePos.y);
    document.getElementById("coords").innerHTML = message;
    var mX = Math.floor(mousePos.x / 20);
    var mY = Math.floor(mousePos.y / 20);
    var index = json[mY - dY][mX - dX];
    document.getElementById("info").innerHTML = (mX-dX) + ' - ' + (mY-dY) + ' - ' + index;
}, false);