import numpy as np
import matplotlib.pyplot as plt
import perlin
import Settings as s
import codecs, json

sn = perlin.SimplexNoise()

def Map(sea_level, oct1, oct2, oct3, oct4, oct5, oct6):
	s.DEM = np.zeros((s.w, s.h), dtype=np.uint8)

	sn.randomize()

	def Noise(x, y):
			octave1 = int(( sn.noise2(x*oct1, y*oct1) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave2 = int(( sn.noise2(x*oct2, y*oct2) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave3 = int(( sn.noise2(x*oct3, y*oct3) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave4 = int(( sn.noise2(x*oct4, y*oct4) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave5 = int(( sn.noise2(x*oct5, y*oct5) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave6 = int(( sn.noise2(x*oct6, y*oct6) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			noise = (octave1 + octave2 + octave3/2 + octave4/10 + octave5/6 + octave6/6)
			return noise

	coords = np.argwhere(s.DEM == 0)


	for x,y in coords:
		s.DEM[x,y] = Noise(x, y)

	s.DEM2 = s.DEM
	s.DEM = s.DEM.astype('float')
	s.DEM[s.DEM <= s.mountain_level-50] = 'nan'

	tile = s.DEM2.tolist()

	json.dump(tile, codecs.open('DEM.json', 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4)

	plt.imshow(s.DEM2)
	plt.axis('off')
	plt.savefig('Tile.png', bbox_inches='tight', pad_inches = 0)
	plt.clf()

Map(100, 0.015625, 0.03125, 0.0625, 0.125, 0.25, 0.8)
